<?php
namespace app\index\controller;
use think\Config;
use think\Env;

class Index
{
    public function index()
    {
    	// 动态配置
    	//Config::set('mystatus','动态配置');

        // 读取所有配置
        //$conf = Config::get();
        // 读取一个配置项
        //$conf = Config::get('default_return_type');
        // 使用助手函数
        //$conf = config('default_return_type');
        // 判断配置项是否存在
        //$has = Config::has('default_return_type');
        // 读取二级配置
        //$conf = Config::get('database.type');

        // 使用助手函数做动态配置
        //config('myconfig','my_php_cn');
        //$conf = Config::get();

        // 批量动态配置
        //Config::set(['config1'=>'phpcn1','config2'=>'phpcn2']);
        //$conf = Config::get();

        // 助手函数批量动态配置
        //config(['config1'=>'phpcn1','config2'=>'phpcn2']);
        //$conf = Config::get();

        // 加载my_config.php中的配置
        //Config::load(APP_PATH.'my_config.php','','test');
        // 加载test作用域下的所有配置
        //$conf = Config::get('','test');

        // 判断test作用域下面是否有type配置项
        //$conf = Config::has('city','test');

        // 加载.env配置项
        $conf = Env::get('envconfig');
        dump($conf);
    }
    
}
