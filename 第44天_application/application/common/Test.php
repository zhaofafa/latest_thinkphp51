<?php
namespace app\common;

//用在Facade门面类静态代理的案例
class Test 
{

	public function hello($name)
	{

		return 'hello,' . $name;

	} 
}