<?php 
namespace app\index\controller;

use app\index\model\Student;

class Demo6
{
	//获取单条记录
	public function get()
	{
		//get()返回当前模型类Student的实例对象
		// dump(Student::get(1));
		dump(Student::get(['id','2']));
		//哪怕用数据库方式查询也是返回模型对象
		// dump(Student::where('id',1)->find());
		//可以检测一下:
		// $obj = Student::where('id',1)->find();
		// dump($obj instanceof Student );  //true
	}

	//获取多条记录
	public function all()
	{

		// dump(Student::all());
		// dump(Student::all([1,2,3]));
		//推荐使用查询构造器,可以执行更加复杂的查询操作
		$res = Student::where('grade','>',80)->field('id,name')->select();
		foreach ($res as $row){
			dump($row);
		}
	}


}