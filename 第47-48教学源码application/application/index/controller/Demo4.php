<?php 
namespace app\index\controller;
use think\Db;

/**
 * 数据库的连接配置
 * 1. 全局配置:config/database.php
 * 2. 动态配置:Db::connect(),connect()方法定义在think\db\Query.php类中
 * 3. DSN连接:数据库类型://用户名:密码@数据库地址:数据库端口/数据库名#字符集
 */
class Demo4
{
	//1. 全局配置:config/database.php
	public function conn1()
	{
		return Db::table('student')
				-> where(['id' => 1])
				-> value('name');
	}

	//2.动态配置:Db::connect()
	public function conn2()
	{
		//先把config/database.php相关配置注释掉
		return Db::connect([
				'type'     => 'mysql',
				'hostname' => 'localhost',
				'database' => 'demo',
				'username' => 'root',
				'password' => 'root',
				'charset'  => 'utf8',
				]) -> table('student')
				   -> where(['id' => 2])
				   -> value('name');
	}

	//3. DSN连接:数据库类型://用户名:密码@数据库地址:数据库端口/数据库名#字符集
	public function conn3()
	{
		//先把config/database.php相关配置注释掉
		$dsn = 'mysql://root:root@localhost:3306/demo#utf8';
		return Db::connect($dsn)
				-> table('student')
				-> where(['id' => 3])
				-> value('name');
	}



}