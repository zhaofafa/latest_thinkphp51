<?php 
namespace app\index\controller;

use think\Db;
/**
 * 查询构造器
 * 一、准备工作:开启二个配置
 * 1. config/app.php
 * 2. 'app_debug'=>true,'app_trace'=> true,
二、将config/database.php参数设置正确
三、学习基本的CURD操作
1. 查询: 单条find(),多条: select()
2. 插入: insert(), 多条: insertAll()
*/


class Demo5 
{
	//1.单条查询
	public function find()
	{	
		/**
		 * Db类是数据库操作的入口类
		 * 功能: 静态调用think\Db\Query.php中的方法实现所有操作
		 * table(完整表名):选择默认数据库的指定表
		 * where(查询条件):设置查询条件,通常使用表达式和数组
		 * 1. 单个条件:推荐使用表达式,通常由三个字符串组成('字段','表达式','值');
		 * 2. 多个条件:推荐使用数组,基本语法:['字段','表达式','值'],默认条件关系是AND
		 * find():返回符合条件的第一条记录,没有找到返回Null
		 */

		$res = Db::table('student')->where('id','=',4)->find();
		dump(is_null($res) ? '没有找到' : $res);
		//如果条件为相等,where('id',4);可省略中间的'='
		// $res = Db::table('student')->where('id',5)->find();
		// dump(is_null($res) ? '没有找到' : $res);
		//如果是根据主键查询,可省略where()条件子句
		// $res = Db::table('student')->find(6);
		// dump(is_null($res) ? '没有找到' : $res);
	}

	//2.多条查询
	public function select()
	{
		
		 //select()找到返回二维数组,没有找到返回空数据
		
		$res = Db::table('student')
			-> field('id,name,course,grade')
			-> where([  //多条件查询用数组实现
				['course','=','php'],
				['grade','>=',80]  //如果是大于90则无数据返回
			])->select();

		if (empty($res)){
			return '没有满足条件的数据';
		} else {
			foreach ($res as $row) {
				dump($row);
			}
		}

	}

	//3.单条插入
	public function insert()
	{
		//insert():成功返回自增主键,失败返回false
		//准备要插入到表中的数据,以数组的方式
		$data = [
			'name'=>'金毛狮王',
			'email'=> 'jmsw@php.cn',
			'course'=>'mysql',
			'grade'=> 80,
			'create_time'=>time(),  //新增时,添加时间与更新时间是一致的
			'update_time'=>time(),
		];

		// return  Db::table('student')->insert($data);
		//如果是MySQL类型,支持replace方式添加INSERT...SET...
		return  Db::table('student')->insert($data,true);
		//使用data()方法包装数据,可以进行一些安全过滤
		// return Db::table('student')->data($data)->insert();
		//如果想在新增数据的同时,返回主键id,不支持data()方法
		// return Db::table('student')->insertGetId($data);

	}

	//4.添加多条
	public function insertAll()
	{
		//insertAll(),添加成功返回条数,失败返回false
		//准备数据,因为是多条数据,需要准备一个二维数组
		//添加数据不需要条件
		$data = [
			['name'=>'朱老师','email'=>'zhu@php.cn','course'=>'PHP'],
			['name'=>'张老师','email'=>'zhang@php.cn','course'=>'Python'],
			['name'=>'李老师','email'=>'li@php.cn','course'=>'JAVA'],
		];

	  	// return Db::table('student')->insertAll($data); 
	  	//支持更新式插入,效率更高,仅限于MySQL 
	  	// return Db::table('student')->insertAll($data,true); //推荐
	  	//同样支持data()方法
	  	return Db::table('student')->data($data)->insertAll($data,true); //推荐
	}

	//5.更新数据
	public function update()
	{
		//update():成功返回更新数据,失败返回false
		//where():必须设置,否则拒绝执行
		//设置更新条件[数组实现]与更新数据
		$map[] = ['id','=',28];
		$data = ['name'=>'西门庆'];
		return Db::table('student')->where($map)->update($data);
		//替换掉原来的更新条件,需要将原来的条件注释掉
		// $map['id']= ['id','=',27];  
		// return Db::table('student')->where($map)->update($data);
		//如果更新条件是主键,可以直接写到更新数据中,会自动识别
		// $data = ['name'=>'西门庆','id'=>25];
		// return Db::table('student')->update($data);

	}

	//6.删除数据
	public function delete()
	{
		//delete():成功返回删除数量,失败返回false
		//与更新一样,必须设置删除条件
		//删除id=28的记录,主键可直接做为参数
		// return Db::table('student')->delete(28); 
		//条件可用where()子句
		return Db::table('student')->where('id',27)->delete();
		//无条件删除所有记录:极危险操作我就不执行了,因为表中数据后面还要用
		// return Db::table('student')->delete(true);
	}

	//7. 原生查询:查询操作
	public function query()
	{
		//调用think\db\Query.php中的query()方法实现
		dump(Db::query("SELECT name,email FROM student WHERE id IN (5,6,9)"));
	}

	//8. 原生查询:添加,更新与删除
	public function execute()
	{
		//调用think\db\Query.php中的execute()方法实现
		//更新操作
		// return Db::execute("UPDATE `student` SET `grade`=99 WHERE `id` = 20");
		//添加操作
		// return Db::execute("INSERT `student` SET `grade`=99,`name`='PeterZhu'");
		//删除操作
		// return Db::execute("DELETE FROM `student` WHERE `id` = 29");
	}

}










