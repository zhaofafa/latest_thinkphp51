<?php 
namespace app\index\controller;
use think\Controller;
use app\model\Student;
use think\facade\View;


class Demo7 extends Controller
{
	//不需要模板,内容直接输出: display()
	public function test1()
	{
		$content = '<h3 style="color:red">PHP中文网欢迎您~~</h3>';
		/**
		 * 直接输出内容到浏览器,使用视图类的display()方法
		 * 在控制器类Controller中已经封装好了display()可直接用
		 * 必须使用return 才可以将内容返回给用户
		 */
		// return $this->display($content);
		//可以使用Controller类中的视图属性来调用
		// return $this->view->display($content);
	
		//使用视图类的静态代理也可以渲染页面
		return View::display($content);  
	}

	//将数据赋值给模板进行输出: assign()和fetch()
	public function test2()
	{
		//模板赋值: assign()
		//1.普通变量
		//单个赋值
		$this->view->assign('name','Peter Zhu');
		$this->view->assign('age',99);
		//批量赋值
		$this->view->assign([
			'sex'=>'男',
			'salary'=>6666
		]);
		//2.数组
		$this->view->assign('goods',[
			'id'=>1,
			'name'=>'手机',
			'model'=>'华为Meta10',
			'price'=>1999
		]);
		//3.对象
		$obj = new \stdClass;
		$obj->course = 'PHP';
		$obj->lecture = 'Peter Zhu';
		$this->view->assign('siteInfo',$obj);

		//4.常量
		define('SITE_NAME', 'PHP中文网');

		//在模板中输出变量
		//模板默认位于当前模板目录下的view目录中,所在目录与控制器同名,模板与方法同名
		
		return $this->view->fetch();
		
	}

	//常用模板标签
	public function test3()
	{
		$data = Student::all();

		$this->view->assign('data', $data);

		return $this->view->fetch();


	}

	//实用技术:分页输出
	public function test4()
	{
		//获取分页数据的方法是Query.php类中的
		//每页显示5条数据
		$data = Student::paginate(5);

		$this->view->assign('data', $data);

		return $this->view->fetch();

	}

	//视图过滤: filter(callback)
	public function test5()
	{
		$content = '我是php中文网的讲师Peter Zhu';
		$callback = function (){
			str_replace(search, replace, subject);
		};

	}
}






