<?php 
namespace  app\index\controller;
use think\Controller;
use app\validate\User;
use think\facade\Validate; //调用验证器的静态代理
// use app\facade\User;  //添加User验证类的静态代理

class Demo9 extends Controller
{
	//1.验证器:使用Validate类的rule属性
	public function test1()
	{
		$data = [
			'name'=> 'Peter',
			'email'=> 'peter@php.cn',
			'password'=>'123abc',
			'mobile'=>18955663322
		];

		$validate = new User;

		if(!$validate->check($data)){
			return $validate->getError();
		} 
			
		return '验证通过';
		
		//使用静态代理实现
		// if(!User::check($data)){
		// 	return User::getError();
		// } 
			
		// return '验证通过';
		
	}

	//使用控制器内置的validate对象
	//$this->validate($data,$validate[,$message])返回验证结果
	//验证通过返回true,否则返回错误信息
	public function test2()
	{
		$data = [
			'name'=> 'Peter',
			'email'=> 'peter@php.cn',
			'password'=>'123abc',
			'mobile'=>18955663322
		];

		//验证规则
		$validate = 'app\validate\User';

		$res = $this->validate($data, $validate);
		if (true !== $res) {
			return $res;
		} 
		return '验证通过';		
	}

	//1.独立验证:使用验证类的:rule()方法
	public function test3()
	{
		//用rule()方法创建验证规则
		$rule = [
			'name|姓名'=> [
				'require'=>'require',
				'max'=>20,
				'alphaDash'=>'alphaDash',//只能是字母数字下划线或破折号			
			],
			'email|邮箱'=>[
				'require'=>'require',
				'email'=>'email',
			],
			'password|密码'=>[
				'require'=>'require',
				'min'=>3,
				'max'=>10,
				'alphaNum'=>'alphaNum',//密码只能是字母或数字
			],
			'mobile|手机'=>[
				'require'=>'require',
				'mobile'=>'mobile',		
		]
	];

	//要验证的数据
	$data = [
			'name'=> 'Peter',
			'email'=> 'peter@php.cn',
			'password'=>'123abc',
			'mobile'=>18955663322
		];

		//添加字段验证规则,实际上初始化$rule属性
		Validate::rule($rule);

		//如果验证不通过直接输出错误信息
		if(!Validate::check($data)){
			return Validate::getError();
		}

		return '验证通过';

	}
}








